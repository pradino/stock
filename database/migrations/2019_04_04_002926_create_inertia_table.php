<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInertiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inertias', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('category');
            $table->string('sku')->unique();
            $table->string('upc');
            $table->string('artist');
            $table->string('title');
            $table->string('format');
            $table->date('release_date');
            $table->decimal('price', 8, 2);
            $table->string('status');
            $table->string('stock');
            $table->string('extra_info')->nullable($value = true);

            $table->boolean('state_changed')->default(0)->comment('Checks if the price, status, stock has been changed from previous entry');
            $table->boolean('is_in_smarttimes')->default(0)->comment('Checks if this product is listed in SmartTimes');
            $table->dateTime('updated_smarttimes_on')->nullable($value = true)->comment('Time when SmartTimes was updated');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('inertia');
    }
}
