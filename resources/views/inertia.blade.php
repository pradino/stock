@extends('layout.base')

@section('title', 'INERTIA - SmartTimes Stock Manager')

@section('content')
    <div class="content">
        <form action="/inertia" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="field  is-grouped">
                <div class="file">
                    <label class="file-label">
                        <input class="file-input" type="file" name="inertiafile">
                        <span class="file-cta">
                        <span class="file-icon">
                            <i class="fas fa-upload"></i>
                        </span>
                        <span class="file-label">
                            Upload the latest stock sheet:
                        </span>
                        </span>
                    </label>
                </div>
                <div class="control">
                    <input type="submit" class="button is-link" value="Upload" />
                </div>
            </div>
        </form>
    </div>

    <div class="content">
        <form action="{{ route('inertia.index') }}">
            <div class="field is-grouped">
                <div class="control is-expanded">
                    <input class="input" type="search" name="q" value="{{ $q }}">
                </div>
            
                <div class="control">
                    <div class="select">
                        <select name="sortBy" class="form-control form-control-sm" value="{{ $sortBy }}">
                            @foreach(['id', 'status', 'stock'] as $col)
                            <option @if($col == $sortBy) selected @endif value="{{ $col }}">{{ ucfirst($col) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            
                <div class="control">
                    <div class="select">
                        <select name="orderBy" class="form-control form-control-sm" value="{{ $orderBy }}">
                            @foreach(['asc', 'desc'] as $order)
                            <option @if($order == $orderBy) selected @endif value="{{ $order }}">{{ ucfirst($order) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            
                <div class="control">
                    <div class="select">
                        <select name="perPage" class="form-control form-control-sm" value="{{ $perPage }}">
                            @foreach(['50','100','250','500'] as $page)
                            <option @if($page == $perPage) selected @endif value="{{ $page }}">{{ $page }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            
                <div class="control">
                <button type="submit" class="button is-primary"">Filter</button>
                </div>
            </div>
        </form>
    </div>
        
    <div class="content is-small">
        <table class="table is-striped is-fullwidth">
            <thead>
                <tr>
                    <th>State</th>
                    <th>sku</th>
                    <th>upc</th>
                    <th>artist</th>
                    <th>title</th>
                    <th>format</th>
                    <th>release_date</th>
                    <th>price</th>
                    <th>status</th>
                    <th>stock</th>
                    <th>Is in ST [Last Updated]</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($items as $item)
                    <tr {{ $item->state_changed ? 'class=is-selected' : '' }}>
                        <th>
                            @switch($item->state_changed)
                                @case(0)
                                    Changed
                                    @break
                                @case(2)
                                    Changed & Updated
                                    @break
                                @default
                                    No Changes
                            @endswitch
                        </th>
                        <td>{{ $item->sku }}</td>
                        <td>{{ $item->upc }}</td>
                        <td>{{ $item->artist }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->format }}</td>
                        <td>{{ $item->release_date }}</td>
                        <td>{{ $item->price }}</td>
                        <td>{{ $item->status }}</td>
                        <td>{{ $item->stock }}</td>
                        <td>{{ $item->is_in_smarttimes ? 'Yes ['. $item->updated_smarttimes_on .']' : 'No' }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $items->links() }}
@endsection