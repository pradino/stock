<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

</head>
<body>
    
    <section class="hero is-primary">
        <div class="hero-head">
                @include('layout.nav')
        </div>
    
        <!-- Hero content: will be in the middle -->
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">@yield('title')</h1>
                {{-- <h2 class="subtitle">Subtitle</h2> --}}
            </div>
        </div>
    </section>

    
    <div class="container is-fluid hero-body">
        @yield('content')
    </div>

    <footer class="footer">
        <div class="content has-text-centered">
            <p><strong>SmartTimes</strong> 2019</p>
        </div>
    </footer>
    {{-- <div>
            @yield('content')
    </div> --}}
    
</body>
</html>