<nav class="navbar">
    <div class="container">
        <div class="navbar-brand">
        <a class="navbar-item">
            <img src="/img/logo.png" alt="Logo">
        </a>
        <span class="navbar-burger burger" data-target="navbarMenuHeroA">
            <span></span>
            <span></span>
            <span></span>
        </span>
        </div>
        <div id="navbarMenuHeroA" class="navbar-menu">
        <div class="navbar-end">
            <a class="navbar-item is-active" href="/">Home</a>
            <a class="navbar-item" href="/inertia">Inertia</a>
        </div>
        </div>
    </div>
</nav>