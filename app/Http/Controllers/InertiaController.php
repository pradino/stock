<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inertia;
use Config;
use Maatwebsite\Excel\Facades\Excel;

class InertiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        $sortBy = 'id';
        $orderBy = 'desc';
        $perPage = 50;
        $q = null;

        if ($request->has('orderBy')) $orderBy = $request->query('orderBy');
        if ($request->has('sortBy')) $sortBy = $request->query('sortBy');
        if ($request->has('perPage')) $perPage = $request->query('perPage');
        if ($request->has('q')) $q = $request->query('q');

        if( $sortBy && $orderBy) {
            $items = Inertia::search($q)->orderBy($sortBy, $orderBy)->paginate($perPage);
            $links = $items->appends(['q' => $q, 'sortBy' => $sortBy, 'orderBy' => $orderBy])->links();
        } else {
            $items = Inertia::paginate($perPage);
            $links = '';
        }
        
        //Excel::import(new \App\Imports\InertiaImport, 'Inertia03042019.xls');
        // $items = Inertia::search($q)->orderBy($sortBy, $orderBy)->paginate($perPage);
        return view('inertia', compact('items', 'orderBy', 'sortBy', 'q', 'perPage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        //echo 'store';
        $file = $request->file('inertiafile');
        //$destinationPath = 'uploads';
        //$file->move($destinationPath,$file->getClientOriginalName());
        Excel::import(new \App\Imports\InertiaImport, request()->file('inertiafile'));

        
        return redirect('/inertia');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function inertiaupload() 
    {
        return 'inertiaupload';
    }
}
