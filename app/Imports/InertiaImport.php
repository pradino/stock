<?php

namespace App\Imports;

use App\Inertia;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Http\Request;
use GuzzleHttp\Client;


class InertiaImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {
            $date_format = floor($row['release_date']);
            $time_format = floatval($row['release_date']) - $date_format;
            $mysql_strdate = ($date_format > 0) ? (( $date_format - 25569 ) * 86400 + $time_format * 86400) :    ($time_format * 86400);

            $check_if_exists = \App\Inertia::firstOrNew(['sku'=>$row['catalogue']]);
            if(!$check_if_exists->exists) {
                \App\Inertia::create([
                    //'category' => $row[''],
                    'sku' => $row['catalogue'],
                    'upc' => $row['upc'],
                    'artist' => $row['artist'],
                    'title' => $row['title'],
                    'format' => $row['format'],
                    'release_date' => date("Y-m-d", $mysql_strdate),
                    'price' => $row['ppd'],
                    'status' => $row['status'],
                    'stock' => $row['stock_on_hand'],
                    'extra_info' => $row['extra_info'],
                ]);
                //exit();
                
            } else {
                \App\Inertia::updateOrCreate(
                    ['sku'=>$row['catalogue']],
                    [
                        'artist' => $row['artist'],
                        'title' => $row['title'],
                        'price' => $row['ppd'],
                        'release_date' => date("Y-m-d", $mysql_strdate),
                        'status' => $row['status'],
                        'stock' => $row['stock_on_hand'],
                    ]
                );
            }
                
            // User::create([
            //     'name' => $row[0],
            // ]);
            
            
        }
        // exit();
    }
}