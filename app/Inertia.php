<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inertia extends Model
{
    //
    protected $fillable = [
        'sku', 'upc', 'artist', 'title', 'format', 'release_date', 'price', 'status', 'stock', 'extra_info'
    ];

    public function scopeSearch($query, $q) 
    {
        if($q == null) return $query;
        return $query
                    ->where('sku', 'LIKE', "%{$q}%")
                    ->orWhere('upc', 'LIKE', "%{$q}%")
                    ->orWhere('artist', 'LIKE', "%{$q}%")
                    ->orWhere('title', 'LIKE', "%{$q}%");
    }
}
